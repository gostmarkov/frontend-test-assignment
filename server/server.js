const express = require("express");
const _ = require("lodash");
const bodyParser = require("body-parser");
var cors = require('cors');

let events = require("./mockEvents");
let nextId = 5;

const port = 3001;
const app = express();
app.use(bodyParser.json())
app.use(cors());


app.get("/events", function (req, res) {
  res.send(events);
});

app.get("/events/:id", function (req, res) {
  const event = _.find(events, {id: parseInt(req.params["id"])});

  if (event) {
    res.send(event);
  } else {
    res.status(404);
    res.send(null);
  }

});

app.post("/events", function (req, res) {
  const event = req.body;

  event.id = nextId;
  nextId += 1;

  events.push(event);

  res.send(event);
});

app.put("/events/:id", function (req, res) {
  const updatedEvent = req.body;
  events = _.map(events, (event) => {
    return event.id === parseInt(req.params["id"])
      ? updatedEvent
      : event;
  });

  res.send(updatedEvent);
});

app.delete("/events/:id", function (req, res) {
  events = _.filter(events, (event) => event.id !== parseInt(req.params["id"]));
  res.send(events);
});


app.listen(port, function () {
  console.log(`Server is running on port ${port}`);
});
