const moment = require("moment");

const mockEvents = [
  {
    id: 1,
    title: "Посадка деревьев",
    dateStart: moment().unix(),
    dateEnd: moment().unix()
  }, {
    id: 2,
    title: "Посадка цветов",
    dateStart: moment().add(3, 'day').unix(),
    dateEnd: moment().add(5, 'day').unix()
  }, {
    id: 3,
    title: "Посадка тыкв",
    dateStart: moment().add(1, 'day').unix(),
    dateEnd: moment().add(3, 'day').unix()
  }
];


module.exports = mockEvents;
