const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const config = {
  target: 'web',
  resolve: {
    extensions: ['.ts', '.js', '.json']
  },
  entry: path.resolve('./src/index.ts'),
  output: {
    path: path.resolve('./build'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: ['awesome-typescript-loader', 'tslint-loader']
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
        loader: "file-loader"
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        use: 'url-loader?limit=1000&name=images/[hash].[ext]'
      },
      {
        test: /\.html$/,
        loader: "ng-cache-loader?prefix=[dir]/[dir]"
      },
      {
        test: /\.css/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  devServer: {
    port: 3000
  },
  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)
    }),
    new webpack.NamedModulesPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve('./src/index.ejs'),
      hash: false,
      favicon: path.resolve('./src/favicon.ico'),
      filename: 'index.html',
      inject: 'body',
      minify: {
        collapseWhitespace: true
      }
    }),
    new webpack.NoEmitOnErrorsPlugin()
  ]
};

const IS_SERVED_BY_DEV_SERVER = process.argv[1].endsWith("webpack-dev-server.js");

if (IS_SERVED_BY_DEV_SERVER) {
  config.devtool = "source-map";
} else {
  config.devtool = "cheap-source-map";
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractTextPlugin({filename: "index.css"})
  );
}

module.exports = config;
