import "es6-shim";
import {module} from "angular";
import "ui-select";
import "angular-sanitize";
import "angular-ui-bootstrap";

import "./index.scss";
import "./main.html"; // put html in angular templateCache

module("app", [
  "ui.select",
  "ngSanitize",
  "ui.bootstrap",
]);
